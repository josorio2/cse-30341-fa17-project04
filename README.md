CSE.30341.FA17: Project 04
==========================

This is the documentation for [Project 04] of [CSE.30341.FA17].

Members
-------

1. Sophie Lancaster (slancas1@nd.edu)
2. Brianna Hoelting (bhoeltin@nd.edu)
3. Josefa Osorio (josorio2@nd.edu)

Design
------

> 1. You will need to implement splitting of free blocks:
>
>   - When should you split a block?
>

We split a block when a request is made that is smaller than the size of any particular free chunk. It is used instead of asking for more memory and allows us to reuse a block of free space. 

>   - How should you split a block?

In order to split a block the allocator finds a free chunk of memory that satisfies the request and then splits it into two. The first chunk is returned to the caller and the second chunk remains in the free list. 

> 2. You will need to implement coalescing of free blocks:
>
>   - When should you coalescing block?
>

When a chunk of memory is freed the allocator will coalesce the free space. This happens if there are contiguous free blocks.

>   - How should you coalesce a block?

Coalescing a block consists of combining consecutive free blocks. When you return a free chunk in memory you should check to see if the that chunk is nearby any other chunks of free space, if the newly-freed space is next to one (or more) existing free chunks they can be merged into one larger free chunk. 

> 3. You will need to implement Next Fit.
>
>   - What information do you need to perform a Next Fit?
>

There are a few pieces of information we will need to perform a Next Fit. We need the block we last searched, the places where all the files are allocated, and the first fit algorithm. 

>   - How would you implement Next Fit?

Next Fit starts off by using a first fit algorithm and then once we need to allocate another block it will do first fit from where it left off. 

> 4. You will need to implement Best Fit.
>
>   - What information do you need to perform a Best Fit?
>

In order to perform Best Fit we need to know the size of the processes to be inserted into memory. 

>   - How would you implement Best Fit?

In order to implement Best Fit will will search through the free list until we find a space that is big enough to fit the best fit.

> 5. You will need to implement Worst Fit.
>
>   - What information do you need to perform a Worst Fit?
>

In order to perform Worst Fit, like Best Fit, we need to know the size of the processes to be inserted into memory.

>   - How would you implement Worst Fit?

In order to implement Worst Fit we will search through the free list for the block that provides the most space for the block to fit into. 

> 6. You will need to implement tracking of different information.
>
>   - What information will you want to track?
>

For this implementation we will want to track the size of the processes and the information for each block such as its size and whether it’s free or not.

>   - How will you update these trackers?
>

We will update these trackers by having functions that can update the information of what is being stored where so that for future purposes we will know not to store something in a block that isn’t free, for example.

>   - How will you report these trackers when the program ends?

In order to report these trackers when the program ends we will have a function that will print out the values of the trackers in the appropriate format. We will use the function atexit to call the function that reports the trackers so that it happens when the program ends.

Demonstration
-------------

> Place a link to your demonstration slides on [Google Drive].

https://docs.google.com/presentation/d/1xbtTVAlHtq1nhfq9BYz85GC4ap25Mu8HhbcHO9ewnzc/edit#slide=id.g27985be151_0_125

Errata
------

> Describe any known errors, bugs, or deviations from the requirements.

Extra Credit
------------

> Describe what extra credit (if any) that you implemented.

[Project 04]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project04.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/
[Google Drive]:     https://drive.google.com
