CC=       	gcc
CFLAGS= 	-g -gdwarf-2 -std=gnu99 -Wall
LDFLAGS=
LIBRARIES=      lib/libmalloc-ff.so \
		lib/libmalloc-nf.so \
		lib/libmalloc-bf.so \
		lib/libmalloc-wf.so

TEST_EXE=	bin/test_00 \
		bin/test_01 \
		bin/test_02 \
		bin/test_03 \
		tests/test_benchmark

all:    $(LIBRARIES) $(TEST_EXE)

test:	$(TEST_EXE)

lib/libmalloc-ff.so:     src/malloc.c
	$(CC) -shared -fPIC $(CFLAGS) -DFIT=0 -o $@ $< $(LDFLAGS)

lib/libmalloc-nf.so:     src/malloc.c
	$(CC) -shared -fPIC $(CFLAGS) -DFIT=1 -o $@ $< $(LDFLAGS)

lib/libmalloc-bf.so:     src/malloc.c
	$(CC) -shared -fPIC $(CFLAGS) -DFIT=2 -o $@ $< $(LDFLAGS)

lib/libmalloc-wf.so:     src/malloc.c
	$(CC) -shared -fPIC $(CFLAGS) -DFIT=3 -o $@ $< $(LDFLAGS)

bin/test_00:	tests/test_00.o
	$(CC) $(CFLAGS) -o $@ $<

bin/test_01:	tests/test_01.o
	$(CC) $(CFLAGS) -o $@ $<

bin/test_02:	tests/test_02.o
	$(CC) $(CFLAGS) -o $@ $<

bin/test_03:	tests/test_03.o
	$(CC) $(CFLAGS) -o $@ $<

tests/test_benchmark:    tests/test_benchmark.o
	$(CC) $(CFLAGS) -o $@ $<

tests/%.o:	tests/%.c
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -f $(LIBRARIES) test/*.o $(TEST_EXE)

.PHONY: all clean
