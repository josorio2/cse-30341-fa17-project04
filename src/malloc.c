/* malloc.c: simple memory allocator -----------------------------------------*/

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>

/* Macros --------------------------------------------------------------------*/

#define ALIGN4(s)           (((((s) - 1) >> 2) << 2) + 4)
#define BLOCK_DATA(b)       ((b) + 1)
#define BLOCK_HEADER(ptr)   ((struct block *)(ptr) - 1)

/* Block structure -----------------------------------------------------------*/

struct block {
    size_t        size;
    struct block *next;
    bool          free;
    bool          used;
};

/* Global variables ----------------------------------------------------------*/

int exit_registered = 0; // used to tell if the program should call atexit or not
struct block *FreeList = NULL; // the free list
struct block *nextfit_ptr = NULL; // block pointer used to keep track of the last searched block
int file_d; // file descriptor

int malloc_count = 0; // tracks the number of mallocs
int free_count = 0; // tracks the number of frees
int reuse_count = 0; // tracks the number of blocks that were resused
int grow_count = 0; // tracks the number of times the heap grew
int split_count = 0; // tracks the number of times a block was split
int coalesce_count = 0; // tracks the number of times blocks were coalesced
int block_count = 0; // tracks the number of blocks in the free list
size_t requested_val = 0; // tracks the number of bytes requested 
size_t max_heap_val = 0; // tracks the max heap size

/* Find free block -----------------------------------------------------------*/

struct block *find_free(struct block **last, size_t size) {
  struct block *curr = FreeList; // sets a block called curr to the beginning of the list 

#if defined FIT && FIT == 0
    /* First fit */
    while (curr && !(curr->free && curr->size >= size)) { // goes through the entire list as long as the block is big enough and sets the last pointer to the current block
      *last = curr;
      curr  = curr->next;
    }
#endif

#if defined FIT && FIT == 1
    /* Next Fit */
    if (nextfit_ptr == NULL) nextfit_ptr = FreeList; // if the last search block is null set the pointer to the head of the list
    curr = nextfit_ptr; // sets curr to the last searched block to start the search here
    // search from current loc to end of list and perform first fit
    while (curr && !(curr->free && curr->size >= size)) {
      *last = curr;
      curr = curr->next;
    }
    // if reached end and didn't find block then start at beginning of list
    if(curr == NULL) {
      curr = FreeList;
      // iterate until reach current pointer
      while (curr && !(curr->free && curr->size >= size) && (curr != nextfit_ptr)) {
        curr = curr->next;
      }
      if(curr == nextfit_ptr) curr = NULL;
    }

    nextfit_ptr = curr;
#endif

#if defined FIT && FIT == 2
    /* Best Fit */
    struct block *best = NULL; // creates a block to keep track of the best fit block
    while (curr) { // iterates through the free list
      if(curr->free && curr->size >= size){ // if statement that enters if the current block is free and the size is greater than or equal to the size we want
        if (!best) best = curr; // sets the best block to the current block
        else if(best->size > curr->size) best = curr; // if the current size of the best block is greater than this block's size set best block to current block
      }
      *last = curr;
      curr = curr->next; // iterate through list
    }
    curr = best;
#endif

#if defined FIT && FIT == 3
    /* Worst Fit */
    struct block *worst = NULL; // creates a block to keep track of the worst fit block
    while (curr) { // iterates through the free list
      if(curr->free && curr->size >= size){ // if statement that enters if the current block is free and the size is greater than or equal to the size we want
        if (!worst) worst = curr; // sets the worst block to the current block
        else if(worst->size < curr->size) worst = curr; // if the current szie of the worst block is less than this block's size set worst block to current block 
      }
      *last = curr;
      curr = curr->next; // iterate through list
    }
    curr = worst;
#endif

    return curr;
}

/* Grow heap -----------------------------------------------------------------*/

struct block *grow_heap(struct block *last, size_t size) {
    /* Request more space from OS */
    struct block *curr = (struct block *)sbrk(0);
    struct block *prev = (struct block *)sbrk(sizeof(struct block) + size);

    assert(curr == prev);

    /* OS allocation failed */
    if (curr == (struct block *)-1) {
        return NULL;
    }

    /* Update FreeList if not set */
    if (FreeList == NULL) {
      FreeList = curr;
      nextfit_ptr = FreeList;
    }

    /* Attach new block to prev block */
    if (last) {
        last->next = curr;
    }

    /* Update block metadata */
    //assert(curr->free == 1);
    //assert(curr->used == 1);
    curr->size = size;
    curr->next = NULL;
    curr->free = false;
    curr->used = false;

    grow_count++;  // increment grow counter
    max_heap_val += size; // increment max heap counter

    return curr;
}

/* Display Tracking Info ------------------------------------------------------*/

void display_tracking() {
  char buffer[BUFSIZ]; // char array that is used to display the tracking information

  struct block *temp = FreeList; // temp block pointer that is used to iterate through the list
  while (temp != NULL) { // iterates through the list and counts the number of blocks
    block_count++;
    temp = temp->next;
    }

  // adds all of the necessary tracking information to the buffer
  sprintf(buffer, "mallocs:   %d\n", malloc_count);
  sprintf(buffer + strlen(buffer), "frees:     %d\n", free_count);
  sprintf(buffer + strlen(buffer), "reuses:    %d\n", reuse_count);
  sprintf(buffer + strlen(buffer), "grows:     %d\n", grow_count);
  sprintf(buffer + strlen(buffer), "splits:    %d\n", split_count);
  sprintf(buffer + strlen(buffer), "coalesces: %d\n", coalesce_count);
  sprintf(buffer + strlen(buffer), "blocks:    %d\n", block_count);
  sprintf(buffer + strlen(buffer), "requested: %zu\n", requested_val);
  sprintf(buffer + strlen(buffer), "max heap:  %zu\n", max_heap_val);
  size_t len = strlen(buffer);

  if (write(file_d, buffer, len) != len) { // writes all of buffer content to the duplicated stdout represented by the file descriptor
      fprintf(stderr, "Failed write\n"); // outputs error message if the write failed
      exit(EXIT_FAILURE);
  }

}

/* Allocate space ------------------------------------------------------------*/

void *malloc(size_t size) {
    /* Align to multiple of 4 */
    size = ALIGN4(size);

    /* Handle 0 size */
    if (size == 0) {
        return NULL;
    }

    // increment requested total counter
    requested_val += size;

    /* Look for free block */
    struct block *last = FreeList;
    struct block *next = find_free(&last, size);

    if (next && next->used) reuse_count++; // incremenets the reuse counter if the block exists and has already been used

    /* Split free block? */
    if(next && next->size > (size + sizeof(struct block))) { // if statement that enters if the size of the next block is greater than the size that we want to malloc
  	  struct block *new_node = (void*)BLOCK_DATA(next) + size; // creates a new block to be placed in the free list

      //assert(new_node->free == 0);
      //assert(new_node->used == 0);
      //assert(next->free == 1);
      //assert(next->used == 0);
  	  new_node->size = next->size - size - sizeof(struct block); // sets the size of the new node to be the size of the current block - the size we want malloced and the size of the block
  	  new_node->free = true; // sets the new node's free object true
  	  new_node->next = next->next; // sets the new node's next to the current node's next
  	  new_node->used = true; // sets the new node's used object to true
  	  next->size = size; // sets the current node's size to the size that was to be allocated
  	  next->free = false; // sets the current node's free object to false
  	  next->used = true; // sets the current node's used object to true
  	  next->next = new_node; // sets the current node's next to the new node so that it is entered in the free list
  	  split_count++; // increment split counter
    }
    /* Could not find free block, so grow heap */
    if (next == NULL) {
      next = grow_heap(last, size);
    }

    /* Could not find free block or grow heap, so just return NULL */
    if (next == NULL) {
        return NULL;
    }

    /* Mark block as in use */
    //assert(next->free == 1);
    //assert(next->used == 0);
    next->free = false;
    next->used = true;

    // increment malloc counter
    malloc_count++;

    /* Return data address associated with block */
    if (!exit_registered) { // if statement that should enter if it is time to exit
      file_d = dup(1); // duplicates standard out to be used for tracking information 
      int i = atexit(display_tracking); // calls atexit and registers the the function that displays the tracking information
      //assert(i != 0);
      if (i != 0) { // if statement that is entered if the atexit call failed
        char errorBuf[BUFSIZ];
        // outputs error message using a buffer, sprintf, and write
        sprintf(errorBuf, "cannot set exit function\n");
        size_t lenErr = strlen(errorBuf);

        if (write(1, errorBuf, lenErr) != lenErr) {
            fprintf(stderr, "Failed write\n");
            exit(EXIT_FAILURE);
        }
        exit(EXIT_FAILURE); // exits with a failure because atexit failed
      }
      else exit_registered = 1; // sets the exit_registered value to 1 so this process only occurs once
    }
    return BLOCK_DATA(next);
}

/* Reclaim space -------------------------------------------------------------*/

void free(void *ptr) {
    if (ptr == NULL) {
        return;
    }

    /* Make block as free */
    struct block *curr = BLOCK_HEADER(ptr);
    assert(curr->free == 0);
    //assert(curr->used == 0);
    curr->free = true;
    curr->used = true;

    free_count++; // increment free counter

    /* Coalesce free blocks */
    while (curr->next && curr->next->free == true) { // goes through the entire free list as long as the next block is free
      curr->size += sizeof(struct block) + curr->next->size; // increments the size of the current block by adding the size of the next block
      curr->next = curr->next->next; // sets the current block's next pointer to the next block's next (this is what "combines" the blocks)
      coalesce_count++; // increments the coalescing counter
    }

}


/* vim: set expandtab sts=4 sw=4 ts=8 ft=cpp: --------------------------------*/
