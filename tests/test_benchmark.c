/* test_00.c: allocate same size block over and over again */

#include <stdlib.h>

#define N       30
#define SIZE    1<<10

int main(int argc, char *argv[]) {
  char *p[N];

  for (int i = 0; i < N; i++) {
    p[i]= malloc(1<<(N - i));
  }

  for (int i = 0; i < N; i+=2){
    free(p[i]);
  }

   for (size_t s = SIZE; s > 0; s>>=1) {
      int *p = malloc(s);
      free(p);
      }

  for (int i = 0; i < N; i++) {
    p[i]= malloc(1<<i);
  }

  for (int i = 1; i < N; i+=2){
    free(p[i]);
  }

  for (int i = 0; i < N; i++) {
    p[i]= malloc(1<<i);
  }

  for (int i = 0; i < N; i+=2){
    free(p[i]);
  }

  for (int i = 0; i < N; i++) {
    p[i]= malloc(1<<(N - i));
  }

  for (int i = 1; i < N; i+=2){
    free(p[i]);
  }

  for (int i = 0; i < N; i++) {
    p[i]= malloc(1<<i);
  }

  for (int i = 0; i < N; i+=2){
    free(p[i]);
  }

  for (int i = 0; i < N; i++) {
    p[i]= malloc(1<<(N - i));
  }

  for (int i = 1; i < N; i+=2){
    free(p[i]);
  }

  for (int i = 0; i < N; i++) {
    p[i]= malloc(1<<(i));
  }

  for (int i = 0; i < N; i+=3){
    free(p[i]);
  }

  for (int i = 0; i < N; i++) {
    p[i]= malloc(1<<(N - i));
  }

  for (int i = 1; i < N; i+=3){
    free(p[i]);
  }

 
  return EXIT_SUCCESS;
}
