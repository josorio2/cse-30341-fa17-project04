/* test_unit_find_free.c */

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#define NDEBUG

#define ALIGN4(s)           (((((s) - 1) >> 2) << 2) + 4)
#define SIZE	1<<10

struct block {
    size_t        size;
    struct block *next;
    bool          free;
    bool          used;
};

struct block *FreeList = NULL;

int main(int argc, char * argv[]){
  size_t s = SIZE;
  s = ALIGN4(s);
  struct block *last = FreeList;

  //tests find free
  struct block *next = find_free(&last, s);
  assert(next != last);

  //tests grow heap
  struct block *grow = grow_heap(&last, s);
  assert(last != grow);

  //tests malloc
  int *p = malloc(s);

  return EXIT_SUCCESS;
}
