#!/bin/bash

test-library() {
    library=$1
    echo -n "Comparing $library output to cat output ... "
    if diff -q <(env LD_PRELOAD=../lib/$library cat test_file.txt | head -n 5) <(test-cat); then
        echo "Success"
    else
        echo "Failure"
        diff -u <(env LD_PRELOAD=../lib/$library cat test_file.txt | head -n 5) <(test-cat)
    fi
	
	echo -n "Comparing $library output to du output ... "
	if diff -q <(env LD_PRELOAD=../lib/$library du test_file.txt | head -n 1) <(test-du); then
		echo "Success"
	else
		echo "Failure"
		diff -u <(env LD_PRELOAD=../lib/$library du test_file.txt | head -n 1) <(test-du)
	fi

	echo -n "Comparing $library output to find output ... "
	if diff -q <(env LD_PRELOAD=../lib/$library find test_file.txt | head -n 1) <(test-find); then
		echo "Success"
	else
		echo "Failure"
		diff -u <(env LD_PRELOAD=../lib/$library find test_file.txt | head -n 1) <(test-find)
	fi

	echo -n "Comparing $library output to md5sum output ... "
	if diff -q <(env LD_PRELOAD=../lib/$library md5sum test_file.txt | head -n 1) <(test-md5sum); then
		echo "Success"
	else
		echo "Failure"
		diff -u <(env LD_PRELOAD=../lib/$library md5sum test_file.txt | head -n 1) <(test-md5sum)
	fi

	echo -n "Comparing $library output to sort output ... "
	if diff -q <(env LD_PRELOAD=../lib/$library sort test_file.txt | head -n 5) <(test-sort); then
		echo "Success"
	else
		echo "Failure"
	diff -u <(env LD_PRELOAD=../lib/$library sort test_file.txt | head -n 5) <(test-sort)
	fi
}

test-cat() {
   cat <<EOF
Hello
How
Are
You
Today?
EOF
}

test-du() {
   cat <<EOF
1	test_file.txt
EOF
}

test-find() {
   cat <<EOF
test_file.txt
EOF
}

test-md5sum() {
   cat <<EOF
3f15a8ae367777cd528121664e018652  test_file.txt
EOF
}

test-sort() {
   cat <<EOF
Are
Hello
How
Today?
You
EOF
}

test-library libmalloc-ff.so
test-library libmalloc-nf.so
test-library libmalloc-bf.so
test-library libmalloc-wf.so
