#!/bin/bash                                                                                                           

test-library() {
    library=$1
    env LD_PRELOAD=./lib/$library cat tests/test_file.txt 
    env LD_PRELOAD=./lib/$library du tests/test_file.txt
    env LD_PRELOAD=./lib/$library find tests/test_file.txt
    env LD_PRELOAD=./lib/$library md5sum tests/test_file.txt
    env LD_PRELOAD=./lib/$library sort tests/test_file.txt
}


test-library libmalloc-ff.so
test-library libmalloc-nf.so
test-library libmalloc-bf.so
test-library libmalloc-wf.so
