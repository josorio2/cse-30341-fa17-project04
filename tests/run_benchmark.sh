#!/bin/bash

test-library() {
    library=$1
    echo -n "Testing $library ... "
    time env LD_PRELOAD=./lib/$library ./tests/test_benchmark
    
}


test-library libmalloc-ff.so
test-library libmalloc-nf.so
test-library libmalloc-bf.so
test-library libmalloc-wf.so